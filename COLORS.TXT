CD_COLOR(3), DS_COLOR(50), HAS_ORIENTATION(1), ALPHA_COLOR(9), FILE_NAME(50)
--- -------------------------------------------------- - --------- -------------------------------------------------- 
W/C SIN COLOR                                          N #00000000                                                   
WHT BLANCO                                             N #FFFBFBFB                                                   
WTX BLANCO TX                                          Y #FFF9F9F9                                                   
MTL METALICO                                           N #FFDFDDE3                                                   
ART ARTICO                                             Y #FFF0F2F9 ARTIC.JPG                                         
ASH CENIZA                                             N #FF6D6769 ASH.JPG                                           
AST FRESNO                                             Y #FF9A8E85 ASH_TREE.JPG                                      
ATT FRESNO TX                                          Y #FFA7A294 ASH_TREE_TX.JPG                                   
BCH HAYA                                               Y #FFE3D9D0 BEECH.JPG                                         
BCT HAYA TX                                            Y #FFE3D9D0 BEECH_TX.JPG                                      
BLL AZUL LN                                            Y #FFAE9EB0 BLUE_LN.JPG                                       
ELM OLMO                                               Y #FFE0DDD8 ELM.JPG                                           
ELT OLMO TX                                            Y #FFB0ACA3 ELM_TX.JPG                                        
FUT FUCSHIA TX                                         Y #FFCF8EA9 FUCSHIA_TX.JPG                                    
GRF GRAFITO                                            N #FF615E63 GRAPHITE.JPG                                      
GRL GRIS LN                                            N #FFC9BBB8 GRAY_LN.JPG                                       
IND ANIL TX                                            N #FF5B7293 INDIGO_TX.JPG                                     
LGR GRIS CLARO                                         N #FF807A75 LIGHT_GRAY.JPG                                    
LIT LILA TX                                            N #FF9C83A0 LILAC_TX.jpg                                      
LMT LIMA TX                                            N #FFAAA661 LIME_TX.JPG                                       
MAL MALVA                                              Y #FFA687A8 MALVA.jpg                                         
MLL MALVA LN                                           Y #FFC7B8BE MALVA_LN.JPG                                      
MPL MAPLE                                              Y #FFFAF3EB MAPLE.JPG                                         
MNK VISON TX                                           N #FFA9A297 MINK_TX.JPG                                       
MKA MOKA                                               Y #FF857276 MOKA.JPG                                          
MSS MUSGO TX                                           N #FFB6D8D8 MOSS_TX.jpg                                       
NCT NACAR TX                                           N #FFFAF7F8 NACRE_TX.JPG                                      
NRD NORDICO                                            N #FFDBD3CC NORDIC.JPG                                        
NRT NORDICO TX                                         N #FFDBD3CC NORDIC_TX.JPG                                     
OBL OCEAN BLUE                                         N #FFABC1DB OCEAN_BLUE.jpg                                    
OLT OLIVA TX                                           N #FFD1D3C7 OLIVE_TX.JPG                                      
ORT NARANJA TX                                         N #FFF3BC92 ORANGE_TX.JPG                                     
OGT OREGON TX                                          N #FFE9E7E3 OREGON_TX.JPG                                     
ORG OREGON                                             N #FFF6F5F3 OREGON.JPG                                        
PKL CALABAZA LN                                        Y #FFF0ECDC PUMPKIN_LN.JPG                                    
PKN CALABAZA                                           N #FFEED9B0 PUMPKIN.jpg                                       
RED ROJO                                               N #FFBD8A86 RED.JPG                                           
REL ROJO LN                                            Y #FFE4CDCC RED_LN.JPG                                        
ROS ROSA                                               N #FFF3DDD9 ROSE.jpg                                          
SND ARENA                                              N #FFC7BCB7 SAND.jpg                                          
SRT PLATA TX                                           N #FFCFCBCB SILVER_TX.JPG                                     
TRT TURQUESA TX                                        N #FFC4DDE7 TURQUOISE_TX.JPG                                  
ULT ULTRAMAR TX                                        N #FFC4C6D3 ULTRAMAR_TX.JPG                                   
WNT NOGAL                                              Y #FFD0C2B2 WALNUT.JPG                                        
BLK BLACK                                              N #FF2F2424                                                   
GRA GRAY                                               N #FFB9B8B8                                                   
CLR CLEAR                                              N #FFF4F1F1                                                   
GRE GREEN                                              N #FF96EC26                                                   
YEL YELLOW                                             N #FFF3F744                                                   
