CD_FAMILY VARCHAR(16), DS_FAMILY VARCHAR(50), CD_FAMILY_PARENT VARRCHAR(16), CD_PROPERTY_GROUP VARCHAR(20)  
---------------- -------------------------------------------------- ---------------- -------------------- 
101              ARMARIOS                                           NULL             ARMARIOS            
102              COMODAS                                            NULL             NULL                
103              CUNAS                                              NULL             NULL                
104              CAMAS ABATIBLES                                    NULL             CAMAS ABATIBLES     
105              MESAS                                              NULL             NULL                
106              ARMARIOS COMBI                                     NULL             NULL                
107              ARMARIOS PUERTAS PLEGABLES                         NULL             NULL                
108              ARMARIOS PEINETA CENTRAL                           NULL             NULL                
109              ARMARIOS PUERTA CORREDERA                          NULL             NULL                
110              ARMARIOS CON LUNAS                                 NULL             NULL                
111              ARMARIOS RINCONEROS                                NULL             NULL                
112              ARMARIOS RINCON REDUCTOR                           NULL             NULL                
113              ARMARIOS PUERTA CURVA                              NULL             NULL                
114              ARMARIOS SUPERFONDO                                NULL             NULL                
115              LIBRERIAS                                          NULL             BOOKSHELFS          
116              LIBRERIAS SOBRE ENCIMERA                           NULL             NULL                
117              CHIFFONIERS                                        NULL             NULL                
118              TERMINALES                                         NULL             NULL                
120              CAJONERAS INTERIORES                               NULL             NULL                
121              ESTANTES INTERIORES                                NULL             NULL                
122              ESTANTES EXTERIORES                                NULL             NULL                
123              COSTADOS DECORATIVOS                               NULL             NULL                
130              PUENTES Y BAJO PUENTES                             NULL             NULL                
140              COMPLEMENTOS PARA NIDOS                            NULL             NULL                
141              NIDOS CON MACIZO DE HAYA                           NULL             NULL                
142              NIDOS MIXTOS                                       NULL             NULL                
143              COMPACTOS                                          NULL             NULL                
144              COMPLEMENTOS BASE COMPACTO                         NULL             NULL                
145              CAMAS COMPACTAS                                    NULL             NULL                
146              ARCONES PARA NIDOS Y COMPACTOS                     NULL             NULL                
147              LITERAS                                            NULL             NULL                
148              ESCALERA LATERAL COMPACTO                          NULL             NULL                
150              CABECEROS DE CAMAS                                 NULL             NULL                
151              BANCADAS                                           NULL             NULL                
152              CAMAS ARC�N                                        NULL             NULL                
160              MESITAS Y COMODINES                                NULL             NULL                
161              MUEBLE AUXILIAR                                    NULL             NULL                
170              ESTANTES ALTOS PARED                               NULL             NULL                
171              ESTANTER�AS                                        NULL             NULL                
172              ENCIMERAS                                          NULL             NULL                
173              MESAS DE DESPACHO                                  NULL             NULL                
200              HERRAJES EN KIT                                    NULL             NULL                
201              HERRAJE                                            NULL             NULL                
202              HERRAJE INCORPORADO                                NULL             NULL                
500              MATERIA PRIMA                                      NULL             NULL                
600              PRODUCTO INTERMEDIO                                NULL             NULL                
700              PREFABRICADOS                                      NULL             NULL                
