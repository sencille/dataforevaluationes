
module InternalCross(){
   translate([0, 0, 0]){
      rotate(-45)
      translate([-0.75, 1, 0])
      square([1.5, 26]);
   }

   translate([20, 0, 0]){
      rotate(45)
      translate([-0.75, 1, 0])
      square([1.5, 26]);
   }
}

module Corner(){
$fn = 100;
   offset(.5) offset(-.5) square([1.5, 7  ]);
   offset(.5) offset(-.5) square([7  , 1.5]);
}

module CornerSquare(){
$fn = 100;
   offset(.5) offset(-.5) square([4.5, 4.5]);
   offset(.5) offset(-.5) square([4.5, 4.5]);
}

module CentralSquare(){
    square(7.52);
}

module CentralRound(){
    circle(d=4.2, $fn=100);
}

module nutProfile20x20(){
    difference(){
        union(){
            InternalCross();
            Corner();
            translate([20,  0, 0]) rotate( 90) Corner();
            translate([20, 20, 0]) rotate(180) Corner();
            translate([ 0, 20, 0]) rotate(270) Corner();

            CornerSquare();
            translate([20-4.5,      0, 0]) CornerSquare();
            translate([20-4.5, 20-4.5, 0]) CornerSquare();
            translate([     0, 20-4.5, 0]) CornerSquare();

            translate([6.25, 6.25, 0]) CentralSquare();
       }
       translate([10, 10, 0]) CentralRound();
    }
}

module ModCAJEADO_BAS_20_OS(){
      nutProfile20x20();
}




